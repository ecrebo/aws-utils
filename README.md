# AWS Utils #

Some useful scripts to make interacting with AWS more convenient.

It has been assumed that the repository is checked out into the directory `~/aws-utils` - feel free to make anything location-agnostic!

## The Scripts ##
### aws-mfa-token ###
Fetch a token for using MFA login.

Usage: `source aws-mfa-token.sh <6 digit MFA token code> [--profile <profile>]`

Note: the script _must_ be `source`ed since it sets environment variables in the current shell.

Also see [the Ecrebo wiki page on the subject](https://ecrebo.atlassian.net/wiki/spaces/DEV/pages/492929059/AWS+CLI+with+Multi-factor+Authentication+MFA)

## The Aliases ##
Command line extensions found in the `aliases` file.

Load with `. aliases`

### updatetoken ###
Calls `aws-mfa-token.sh` to login with MFA.

Usage: `updatetoken <6 digit MFA token code>`

