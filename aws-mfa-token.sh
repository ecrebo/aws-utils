#!/bin/bash
if [[ "$0" = "$BASH_SOURCE" ]]
then
  echo -e "ERROR - you have to 'source' the script for it to work.\nUsage:\n source aws-mfa-token.sh <6 digits MFA token code> [--profile internal]"
elif [[ ! "$1" =~ [0-9]{6} ]]
then
  echo -e "Usage:\n source aws-mfa-token.sh <6 digits MFA token code> [--profile internal]"
else
  TOKEN=$1
  shift
  PROFILE=$@
  unset AWS_SESSION_TOKEN
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
 
  # Get User Serial Number
  AWS_MFA_DEVICE_SERIAL_NO=$(aws iam list-mfa-devices --output text --query 'MFADevices[0].SerialNumber' $PROFILE)
 
  if [ "$AWS_MFA_DEVICE_SERIAL_NO" != "" ]
  then
    # get temporary token using AWS API
    CREDENTIAL=$(aws sts get-session-token --output text --serial-number "$AWS_MFA_DEVICE_SERIAL_NO" --token-code "$TOKEN"  --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken,Expiration]' $PROFILE)
    if [ "$CREDENTIAL" != "" ]
    then
      export AWS_ACCESS_KEY_ID=$(echo $CREDENTIAL | cut -d' ' -f1)
      export AWS_SECRET_ACCESS_KEY=$(echo $CREDENTIAL | cut -d' ' -f2)
      export AWS_SESSION_TOKEN=$(echo $CREDENTIAL | cut -d' ' -f3)
      echo "All set. Your token expires on $(echo $CREDENTIAL | cut -d' ' -f4)."
    fi
  fi
fi
